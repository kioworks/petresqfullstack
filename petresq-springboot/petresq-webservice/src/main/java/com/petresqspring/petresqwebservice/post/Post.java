package com.petresqspring.petresqwebservice.post;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Post {
    @Id
    @GeneratedValue
    private Long id;
    private String username;
    private String description;
    private Date submitDate;
    private boolean foundHome;

    public Post() {

    }

    public Post(long id, String username, String description, Date submitDate, boolean foundHome) {
        super();
        this.id = id;
        this.username = username;
        this.description = description;
        this.submitDate = submitDate;
        this.foundHome = foundHome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTargetDate() {
        return submitDate;
    }

    public void setTargetDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public boolean foundHome() {
        return foundHome;
    }

    public void setFoundHome(boolean foundHome) {
        this.foundHome = foundHome;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Post other = (Post) obj;
        if (id != other.id)
            return false;
        return true;
    }


}