package com.petresqspring.petresqwebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetresqWebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetresqWebserviceApplication.class, args);
	}

}
