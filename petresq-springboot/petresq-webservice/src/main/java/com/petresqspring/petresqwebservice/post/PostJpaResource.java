package com.petresqspring.petresqwebservice.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@CrossOrigin(origins="http://localhost:4200")
@RestController
public class PostJpaResource {

    @Autowired
    private PostHardcodedService postService;

    @Autowired
    private PostJpaRepository postJpaRepository;


    @GetMapping("/jpa/users/{username}/posts")
    public List<Post> getAllPosts(@PathVariable String username){
        return postJpaRepository.findByUsername(username);
        //return postService.findAll();
    }

    @GetMapping("/jpa/users/{username}/posts/{id}")
    public Post getPost(@PathVariable String username, @PathVariable long id){
        return postJpaRepository.findById(id).get();
        //return postService.findById(id);
    }

    // DELETE /users/{username}/posts/{id}
    @DeleteMapping("/jpa/users/{username}/posts/{id}")
    public ResponseEntity<Void> deletePost(
            @PathVariable String username, @PathVariable long id) {

        postJpaRepository.deleteById(id);

        return ResponseEntity.noContent().build();
    }


    //Edit/Update a Post
    //PUT /users/{user_name}/posts/{post_id}
    @PutMapping("/jpa/users/{username}/posts/{id}")
    public ResponseEntity<Post> updatePost(
            @PathVariable String username,
            @PathVariable long id, @RequestBody Post post){

        post.setUsername(username);

        Post postUpdated = postJpaRepository.save(post);

        return new ResponseEntity<Post>(post, HttpStatus.OK);
    }

    @PostMapping("/jpa/users/{username}/posts")
    public ResponseEntity<Void> createPost(
            @PathVariable String username, @RequestBody Post post){

        post.setUsername(username);

        Post createdPost = postJpaRepository.save(post);

        //Location
        //Get current resource url
        ///{id}
        URI uri =  ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(createdPost.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

}